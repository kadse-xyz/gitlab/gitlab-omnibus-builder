# Debian bullseye patched `ld` to default to `--as-needed`, but this can
# cause issues with jemalloc (https://bugs.ruby-lang.org/issues/18409).
#
# Raspberry Pi builds may also need to link in libatomic to support
# armv6 platforms, and `--as-needed` causes this dependency to be
# dropped in the shared libraries used in C extensions. This causes
# missing symbols during runtime.
#
# These flags are already included in Ruby 3.0.4 (https://github.com/ruby/ruby/pull/4631)
# when --enabled shared is used.
ENV RUBY_EXTRA_LDFLAGS='-Wl,--no-as-needed'

RUN curl -fsSL "https://cache.ruby-lang.org/pub/ruby/${RUBY_MINOR_VERSION}/ruby-${RUBY_VERSION}.tar.gz" \
    | tar -xzC /tmp \
    && cd /tmp/ruby-${RUBY_VERSION} \
    && ./autogen.sh \
    && LDFLAGS=${RUBY_EXTRA_LDFLAGS} ./configure --disable-install-rdoc --disable-install-doc --disable-install-capi --host=arm-linux-gnueabihf --target=arm-linux-gnueabihf --build=arm-linux-gnueabihf ${RUBY_CONFIGURE_EXTRA_OPTS} \
    && make \
    && make install \
    && cd / && rm -rf /tmp/ruby-${RUBY_VERSION}

RUN /usr/local/bin/gem update --system ${RUBYGEMS_VERSION} --no-document

RUN /usr/local/bin/gem install bundler --version ${BUNDLER_VERSION} --no-document

RUN /usr/local/bin/gem install license_finder --version ${LICENSE_FINDER_VERSION} --no-document

#!/bin/sh

# Disable automatic CoreOS updates
echo "Disabling automatic updates"
sudo systemctl mask update-engine.service
sudo systemctl mask locksmithd.service


# Disable password ssh auth
echo "Disabling password ssh auth"
if [ "$(readlink -f /etc/ssh/sshd_config)" = '/usr/share/google-oslogin/sshd_config' ]; then
	echo '/etc/ssh/sshd_config is a symlink to /usr/share/google-oslogin/sshd_config. Unlinking before write.'
	sudo cp --remove-destination /usr/share/google-oslogin/sshd_config /etc/ssh/sshd_config
fi
sudo tee -a /etc/ssh/sshd_config > /dev/null << EOF
PasswordAuthentication no
PermitRootLogin no
ChallengeResponseAuthentication no
EOF
